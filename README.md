# Autolast fra statbank

Dette er work in progress.

`fetch_data.py` inneholder noen hjelpefunksjoner for å generere spørring og hente hele data (tabeller) fra et tabellnummer. Dette fungerer ganske greit, så lenge tabellen inneholder færre enn 800 000 celler.

`query_helper.py` er et forsøk på å lage en klasse av det hele, inkludert noen underlige forsøk på å splitte opp en spørring i flere blokker dersom tabellen har mer enn 800 000 celler. Flere ting i denne klassen er litt på skakke akkurat nå, kort fortalt ikke bruk denne enda.

En del allerede nedlastede tabeller ligger i `/data` mappen.