import json
import math
import requests
import itertools

def total_cells(j):
    """
    Returns total number of cells in a table. The SSB API returns a maximum of 300 000 cells,
    so if the total number of cells is higher than this we need to split the table

    :param j: JSON (dict) conforming to the SSB API metadata format
    :return: Float total number of cells
    """
    vars = j['variables']
    lenghts = [len(elm['valueTexts']) for elm in vars]
    cellsum = math.exp(sum([math.log(length) for length in lenghts]))
    return cellsum

def get_api_metadata(tablenum):
    """
    Fetch metadata for a given table-number (5-digit unique number)
    :param tablenum: 5-digit string
    :return: metadata dictionary
    """

    returntxt = requests.get(f"https://data.ssb.no/api/v0/no/table/{tablenum}").text

    return json.loads(returntxt)


def create_full_request(j):
    """
    Creates full POST request string for API call.
    :param j: SSB metadata dict
    :return: json-stat2 formatted dictionary
    """

    q_dict = { "query": [],
              "response": {"format": "json-stat2"}
    }

    for element in j['variables']:
        q_part = {
            "code": element['code'], "selection": {
            "filter": "all",
            "values": ["*"]
            }
        }

        q_dict['query'].append(q_part)

    return q_dict

def fetch_data(tablenum, q_dict):
    """
    Return dataset for given table-number and query

    :param tablenum: 5-digit string
    :param q_dict: Valid dictionary conforming to SSB API definition
    :return: JSON dict (json-stat2 format) with data
    """

    rsp = requests.post(f"https://data.ssb.no/api/v0/no/table/{tablenum}", json=q_dict)

    print(rsp.status_code)
    return rsp.text



def split_call(j):

    CELLIMIT = 50 # 300_000
    table_cells = total_cells(j)
    if table_cells < CELLIMIT:
        return [j]

    vars = j['variables']
    lenghts = [table_cells/len(elm['valueTexts']) for elm in vars]



def master_fetcher(tabnum, outfile):
    """
    Putting it all together

    :param tabnum: 5-digit string with table number
    :return: something?
    """

    mtd = get_api_metadata(tabnum)

    if total_cells(mtd) < 800_000:
        post_data = create_full_request(mtd)
        jsdata = fetch_data(tabnum, post_data)
        with open(outfile, 'w') as f:
            f.write(jsdata)
        f.close()
    else:
        raise Exception("Too many cells for table", tabnum)
    return True




from time import sleep
oversized_tables = ["07459", "09817", "12882"] # Omitted/customized query due to size

tables = ["05471", "09588", "04231", "08425", "11786", "11792", "09475", "08243", "08487", "12044", "12209", "11993", "11046", "06083", "12980"]
failed = []
for tablenum in tables:
    try:
        # Write query-file
        mtd = get_api_metadata(tablenum)
        post_data = create_full_request(mtd)
        with open(f"queries/{tablenum}_query.json", 'w') as f:
            f.write(json.dumps(post_data, indent=4, sort_keys=True))
        f.close()

        #master_fetcher(tablenum, f"data/{tablenum}.json")
        sleep(3)
    except Exception as e:
        print(tablenum, e)
        failed.append(tablenum)


#
#
#
#
# tab = "07459"
# mtd = get_api_metadata(tab)
# post_data = create_full_request(mtd)
# post_data
#
# post_data = {'query': [{'code': 'Region', 'selection': {'filter': 'item', 'values': ['3805', '38']}},
#                        {'code': 'Kjonn', 'selection': {'filter': 'all', 'values': ['*']}},
#                        {'code': 'Alder', 'selection': {'filter': 'all', 'values': ['*']}},
#                        {'code': 'ContentsCode', 'selection': {'filter': 'all', 'values': ['*']}},
#                        {'code': 'Tid', 'selection': {'filter': 'all', 'values': ['*']}}
#                        ], 'response': {'format': 'json-stat2'}}
#
#
# jsdata = fetch_data(tab, post_data)
#
#
# with open(f"data/{tab}_larvik.json", 'w') as f:
#     f.write(jsdata)
# f.close()
#
#
# mtd
#
# total_cells(mtd)
#
#
# 3805
#
# post_data
#
# qstr = {'query': [{'code': 'Region', 'selection': {'filter': 'all', 'values': ['3805', '38']}}, {'code': 'InnvandrKat', 'selection': {'filter': 'all', 'values': ['*']}}, {'code': 'Landbakgrunn', 'selection': {'filter': 'all', 'values': ['*']}}, {'code': 'ContentsCode', 'selection': {'filter': 'all', 'values': ['*']}}, {'code': 'Tid', 'selection': {'filter': 'all', 'values': ['*']}}], 'response': {'format': 'json-stat2'}}
# json.dumps(qstr)
# #post_data = create_full_request(mtd)
# jsdata = fetch_data("09817", qstr)
#
# with open("data/07459.json", 'w') as f:
#     f.write(jsdata)
# f.close()