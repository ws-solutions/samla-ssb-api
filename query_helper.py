import json
import math
import copy



class statbank_query():

    Q_BASE = {"query": [],
          "response": {"format": "json-stat2"}
                  }

    data_query = {"query": [],
          "response": {"format": "json-stat2"}
                  }

    q_array = []
    tabledef = None
    tablenum = None
    n_vars = None
    n_cells = None
    variable_range = None
    variable_lengths = None
    data = None
    data_array = []
    MAX_CELLS = 800_000

    def __init__(self, tablenum):
        self.tablenum = tablenum

    def set_metadata(self, tabledef):
        self.tabledef = tabledef
        self.n_vars = len(tabledef['variables'])
        self.variable_range = range(0, self.n_vars)
        self.n_cells = self.total_cells(tabledef)
        self.variable_lengths = [len(elm['valueTexts']) for elm in tabledef['variables']]
        return True

    def fetch_metadata(self, return_json=False):
        """
        Fetch metadata for a given table-number (5-digit unique number)
        :param return_json: Bool if the metadata json object gets returned
        :return: metadata dictionary
        """

        returntxt = requests.get(f"https://data.ssb.no/api/v0/no/table/{self.tablenum}").text

        self.tabledef = json.loads(returntxt)

        self.n_vars = len(self.tabledef['variables'])
        self.n_cells = self.total_cells(self.tabledef)
        self.variable_range = range(0, self.n_vars)
        self.variable_lengths = [len(elm['valueTexts']) for elm in self.tabledef['variables']]

        if return_json:
            return self.tabledef
        else:
            return True

    def total_cells(self, tabledef):
        """
        Returns total number of cells in a table. The SSB API returns a maximum of 300 000 cells,
        so if the total number of cells is higher than this we need to split the table

        :param j: JSON (dict) conforming to the SSB API metadata format
        :return: Float total number of cells
        """
        vars = tabledef['variables']
        lenghts = [len(elm['valueTexts']) for elm in vars]
        cellsum = math.exp(sum([math.log(length) for length in lenghts]))
        return cellsum

    def get_number_of_cells(self):
        return self.n_cells

    def generate_query(self):

        for element in self.tabledef['variables']:
            q_part = {
                "code": element['code'], "selection": {
                    "filter": "all",
                    "values": ["*"]
                }
            }

            self.data_query['query'].append(q_part)

        return True

    def generate_query_list(self):

        if len(self.q_array)>0:
            raise Exception("Cowardly aborting because there is already a query array here")

        possible_combos = self.get_possible_combos()
        combo_length = self.get_combo_lengths(possible_combos)
        combo_sizes = self.get_combo_sizes(combo_length)

        if max(combo_sizes)==0:
            raise Exception("Need to split more than 1 variable, not supported yet")

        full_vars = self.get_unsliced_vars(combo_sizes, possible_combos)

        split_vars = self.get_split_vars(full_vars)
        print(len(split_var))
        print(split_vars)
        for var in full_vars:

            q_part = {
                "code": var['code'], "selection": {
                    "filter": "all",
                    "values": ["*"]
                }
            }
            self.data_query['query'].append(q_part)

        for single_var in split_vars:
            var_elements = self.create_var_query_element(single_var)
            for code in var_elements:
                q_dict_part = copy.deepcopy(self.data_query)
                q_dict_part['query'].append(code)
                self.q_array.append(q_dict_part)

        return True

    def print_query_list(self):
        print(json.dumps(self.q_array, indent=4, sort_keys=True))

    def print_query(self):
        print(json.dumps(self.data_query, indent=4, sort_keys=True))

    def get_possible_combos(self):
        possible_combos = list(itertools.combinations(self.variable_range, self.n_vars - 1))
        return possible_combos

    def get_combo_lengths(self, possible_combos):
        lengths = [get_subset_from_index_tuple(self.variable_lengths, cells) for cells in possible_combos]
        return lengths

    def get_combo_sizes(self, combo_length):
        combo_sizes = [self.tupleproduct(t) if self.tupleproduct(t) > self.MAX_CELLS else 0 for t in combo_length]
        return combo_sizes

    def get_unsliced_vars(self, combo_sizes, possible_combos):
        full_vars_indexes = possible_combos[combo_sizes.index(max(combo_sizes))]
        full_vars = get_subset_from_index_tuple(self.tabledef['variables'], full_vars_indexes)
        return full_vars

    def get_split_vars(self, full_vars):
        split_var = [item for item in self.tabledef['variables'] if item not in full_vars]
        return split_var

    def tupleproduct(self, tpl):
        logvalues = [math.log(t) for t in tpl]
        expsumlog = math.exp(sum(logvalues))
        return expsumlog

    def get_subset_from_index_tuple(self, valuelist, indextpl):
        indexlist = list(indextpl)
        items = [valuelist[k] for k in indexlist]
        return items

    def create_var_query_element(self, var):
        elements = []
        for k, v in zip(var['values'], var['valueTexts']):
            elements.append({"code": var['code'], "selection": {"filter": "all", "values": [k]}})

        return elements

    def fetch_data_list(self):
        for query in self.q_array:
            rsp = requests.post(f"https://data.ssb.no/api/v0/no/table/{self.tablenum}", json=query)

            if not rsp.ok:
                raise Exception(f"Query returned with http status code {rsp.status_code}")
            else:
                self.data_array.append(json.loads(rsp.text))

            sleep(3)

        return True

    def fetch_data(self):
        if self.n_cells>self.MAX_CELLS:
            raise Exception("Too many cells for one query. You probably need to run generate_query_list")

        rsp = requests.post(f"https://data.ssb.no/api/v0/no/table/{self.tablenum}", json=self.data_query)

        if not rsp.ok:
            raise Exception(f"Query returned with http status code {rsp.status_code}")
        else:
            self.data = json.loads(rsp.text)

        return True

    def write_data(self, outfile):

        with open(outfile, 'w') as f:
            f.write(json.dumps(self.data))
        f.close()

        return True

    def write_data_list(self, outfile):

        for i, datapart in enumerate(data_array):
            with open(f"{outfile}_{i}", 'w') as f:
                f.write(self.datapart)
            f.close()

        return True



